class Employee {
    constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
    }

    get name() {
    return this._name;
    }

    get age() {
    return this._age;
    }

    get salary() {
    return this._salary;
    }

    set salary(value) {
    this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
    }

    get salary() {
      return this._salary * 3;
    }

    get lang() {
    return this._lang;
    }

    set lang(value) {
    this._lang = value;
    }
}

let programmer1 = new Programmer("John", 25, 50000, ["Python", "JavaScript"]);
let programmer2 = new Programmer("Alice", 30, 60000, ["Java", "JavaScript"]);
let programmer3 = new Programmer("Bob", 28, 55000, ["C#", "JavaScript"]);

console.log("Programmer 1:");
console.log("Name:", programmer1.name);
console.log("Age:", programmer1.age);
console.log("Salary:", programmer1.salary);
console.log("Languages:", programmer1.lang);

console.log("Programmer 2:");
console.log("Name:", programmer2.name);
console.log("Age:", programmer2.age);
console.log("Salary:", programmer2.salary);
console.log("Languages:", programmer2.lang);

console.log("Programmer 3:");
console.log("Name:", programmer3.name);
console.log("Age:", programmer3.age);
console.log("Salary:", programmer3.salary);
console.log("Languages:", programmer3.lang);